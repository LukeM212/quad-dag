export default class InternPool<T> {
	private pool: InternTree<T> = new InternTree();
	
	constructor(
		private toList: (obj: T) => any[],
		private toRelease: (obj: T) => T[],
	) { }
	
	public make(obj: T): T {
		const interned = this.pool.make(this.toList(obj), obj);
		if (interned !== obj)
			for (const value of this.toRelease(obj))
				this.release(value);
		return interned;
	}
	
	public copy(obj: T) {
		this.pool.copy(this.toList(obj));
		return obj;
	}
	
	public release(obj: T) {
		if (this.pool.release(this.toList(obj)))
			for (const value of this.toRelease(obj))
				this.release(value);
	}
}

class InternTree<T> {
	private count: number = 0;
	private value: T | null = null;
	private children: Map<any, InternTree<T>> = new Map();
	
	public make(keys: any[], obj: T): T {
		if (keys.length === 0) {
			if (this.count === 0) this.value = obj;
			this.count += 1;
			return this.value!;
		} else {
			let child = this.children.get(keys[0]);
			if (child === undefined) this.children.set(keys[0], child = new InternTree());
			return child.make(keys.slice(1), obj);
		}
	}
	
	public copy(keys: any[]) {
		if (keys.length === 0) this.count += 1;
		else this.children.get(keys[0])!.copy(keys.slice(1));
	}
	
	public release(keys: any[]): boolean {
		if (keys.length === 0) {
			this.count -= 1;
			return this.count === 0;
		} else {
			const child = this.children.get(keys[0])!;
			const released = child.release(keys.slice(1));
			if (child.count === 0 && child.children.size === 0)
				this.children.delete(keys[0]);
			return released;
		}
	}
}
