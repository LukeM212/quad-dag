export default class MinMaxHeap {
	private readonly heap: number[] = [];
	
	public get size(): number {
		return this.heap.length;
	}
	
	public get min(): number | null {
		if (this.heap.length === 0) return null;
		else return this.heap[0];
	}
	
	public get max(): number | null {
		if (this.heap.length === 0) return null;
		else if (this.heap.length === 1) return this.heap[0];
		else if (this.heap.length === 2) return this.heap[1];
		else return Math.max(this.heap[1], this.heap[2]);
	}
	
	private sift(i: number) {
		if (Math.floor(Math.log2(i + 1)) % 2 === 0)
			this.siftMin(i);
		else this.siftMax(i);
	}
	
	private siftMin(i: number) {
		const c1 = i * 2 + 1;
		if (c1 < this.heap.length) {
			let min = i;
			const c11 = c1 * 2 + 1;
			if (c11 < this.heap.length) {
				if (this.heap[c11] < this.heap[min]) min = c11;
				const c12 = c11 + 1;
				if (c12 < this.heap.length) {
					if (this.heap[c12] < this.heap[min]) min = c12;
				}
			} else {
				if (this.heap[c1] < this.heap[min]) min = c1;
			}
			const c2 = c1 + 1;
			if (c2 < this.heap.length) {
				const c21 = c2 * 2 + 1;
				if (c21 < this.heap.length) {
					if (this.heap[c21] < this.heap[min]) min = c21;
					const c22 = c21 + 1;
					if (c22 < this.heap.length) {
						if (this.heap[c22] < this.heap[min]) min = c22;
					}
				} else {
					if (this.heap[c2] < this.heap[min]) min = c2;
				}
			}
			if (min > i) {
				const tmp = this.heap[i];
				this.heap[i] = this.heap[min];
				this.heap[min] = tmp;
				if (min > c2) {
					const parent = Math.floor((i + 1) / 2) - 1;
					if (tmp > this.heap[parent]) {
						this.heap[min] = this.heap[parent];
						this.heap[parent] = tmp;
					}
					this.siftMin(min);
				}
			}
		}
	}
	
	private siftMax(i: number) {
		const c1 = i * 2 + 1;
		if (c1 < this.heap.length) {
			let min = i;
			const c11 = c1 * 2 + 1;
			if (c11 < this.heap.length) {
				if (this.heap[c11] > this.heap[min]) min = c11;
				const c12 = c11 + 1;
				if (c12 < this.heap.length) {
					if (this.heap[c12] > this.heap[min]) min = c12;
				}
			} else {
				if (this.heap[c1] > this.heap[min]) min = c1;
			}
			const c2 = c1 + 1;
			if (c2 < this.heap.length) {
				const c21 = c2 * 2 + 1;
				if (c21 < this.heap.length) {
					if (this.heap[c21] > this.heap[min]) min = c21;
					const c22 = c21 + 1;
					if (c22 < this.heap.length) {
						if (this.heap[c22] > this.heap[min]) min = c22;
					}
				} else {
					if (this.heap[c2] > this.heap[min]) min = c2;
				}
			}
			if (min > i) {
				const tmp = this.heap[i];
				this.heap[i] = this.heap[min];
				this.heap[min] = tmp;
				if (min > c2) {
					const parent = Math.floor((i + 1) / 2) - 1;
					if (tmp < this.heap[parent]) {
						this.heap[min] = this.heap[parent];
						this.heap[parent] = tmp;
					}
					this.siftMax(min);
				}
			}
		}
	}
	
	private bubble(i: number) {
		if (i > 0) {
			const value = this.heap[i];
			const newI = Math.floor((i + 1) / 2) - 1;
			const parent = this.heap[newI];
			if (Math.floor(Math.log2(i + 1)) % 2 === 0) {
				if (value > parent) {
					this.heap[i] = parent;
					this.heap[newI] = value;
					this.bubbleMax(newI);
				} else this.bubbleMin(i);
			} else {
				if (value < parent) {
					this.heap[i] = parent;
					this.heap[newI] = value;
					this.bubbleMin(newI);
				} else this.bubbleMax(i);
			}
		}
	}
	
	private bubbleMin(i: number) {
		if (i > 2) {
			const value = this.heap[i];
			const newI = Math.floor((i + 1) / 4) - 1;
			const parent = this.heap[newI];
			if (value < parent) {
				this.heap[i] = parent;
				this.heap[newI] = value;
				this.bubbleMin(newI);
			}
		}
	}
	
	private bubbleMax(i: number) {
		if (i > 6) {
			const value = this.heap[i];
			const newI = Math.floor((i + 1) / 4) - 1;
			const parent = this.heap[newI];
			if (value > parent) {
				this.heap[i] = parent;
				this.heap[newI] = value;
				this.bubbleMax(newI);
			}
		}
	}
	
	private take(i: number): number {
		const value = this.heap[i];
		this.heap[i] = this.heap[this.heap.length - 1];
		this.heap.pop();
		this.sift(i);
		return value;
	}
	
	public takeMin(): number | null {
		if (this.heap.length === 0) return null;
		else return this.take(0);
	}
	
	public takeMax(): number | null {
		if (this.heap.length === 0) return null;
		else if (this.heap.length === 1) return this.take(0);
		else if (this.heap.length === 2) return this.take(1);
		else if (this.heap[1] > this.heap[2]) return this.take(1);
		else return this.take(2);
	}
	
	public add(n: number) {
		this.bubble(this.heap.push(n) - 1);
	}
}

// 5, 10, 3, 2 fails
