import MinMaxHeap from './MinMaxHeap';

export default class NumberGenerator {
	private readonly available = new MinMaxHeap();
	private next = 0;
	
	public assign(): number {
		return this.available.takeMin() ?? this.next++;
	}
	
	public release(n: number) {
		this.available.add(n);
		while (this.available.max === this.next - 1)
			this.next = this.available.takeMax()!;
	}
}
