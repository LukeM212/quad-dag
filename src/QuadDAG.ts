import InternPool from './InternPool';

export type QDAGNode = {
	readonly tl: QDAGNode | null,
	readonly tr: QDAGNode | null,
	readonly bl: QDAGNode | null,
	readonly br: QDAGNode | null,
};

export default class QuadDAG {
	private nodePool: InternPool<QDAGNode> = new InternPool(
		({tl, tr, bl, br}) => [tl, tr, bl, br],
		({tl, tr, bl, br}) => [tl, tr, bl, br].filter(x => x !== null) as QDAGNode[],
	);
	
	protected root: QDAGNode | null = null;
	
	constructor(
		public readonly size: number,
	) { }
	
	public set(
		x: number, y: number,
		set: boolean,
	) {
		const newNode = this.setNode(x, y, set, this.root, this.size);
		if (this.root !== null) this.nodePool.release(this.root);
		this.root = newNode;
	}
	
	private setNode(
		x: number, y: number,
		set: boolean,
		node: QDAGNode | null, size: number,
	): QDAGNode | null {
		if (size === 1) {
			if (set && node === null) return this.nodePool.make({tl: null, tr: null, bl: null, br: null});
			else if (!set && node !== null) return null;
		} else {
			const make = (node: QDAGNode) => {
				const {tl, tr, bl, br} = node;
				if (tl == null && tr == null && bl == null && br == null) return null;
				else return this.nodePool.make(node);
			};
			const copy = (node: QDAGNode | null) => {
				if (node !== null) this.nodePool.copy(node);
				return node;
			}
			
			const newSize = size / 2;
			const {tl, tr, bl, br} = node ?? {tl: null, tr: null, bl: null, br: null};
			if (x < newSize && y < newSize) {
				const newNode = this.setNode(x, y, set, tl, newSize);
				if (newNode !== tl) return make({tl: newNode, tr: copy(tr), bl: copy(bl), br: copy(br)});
			} else if (x >= newSize && y < newSize) {
				const newNode = this.setNode(x - newSize, y, set, tr, newSize);
				if (newNode !== tr) return make({tl: copy(tl), tr: newNode, bl: copy(bl), br: copy(br)});
			} else if (x < newSize && y >= newSize) {
				const newNode = this.setNode(x, y - newSize, set, bl, newSize);
				if (newNode !== bl) return make({tl: copy(tl), tr: copy(tr), bl: newNode, br: copy(br)});
			} else if (x >= newSize && y >= newSize) {
				const newNode = this.setNode(x - newSize, y - newSize, set, br, newSize);
				if (newNode !== br) return make({tl: copy(tl), tr: copy(tr), bl: copy(bl), br: newNode});
			}
		}
		
		// No change, return old node
		if (node === null) return null;
		else return this.nodePool.copy(node);
	}
}
