import QuadDAG, {QDAGNode} from './QuadDAG';
import NumberGenerator from './NumberGenerator';

const gRatio = (Math.sqrt(5) - 1) / 2;

export default class VisualQuadDAG extends QuadDAG {
	constructor(
		size: number,
	) { super(size); }
	
	public draw(
		canvas: HTMLCanvasElement,
	) {
		const ctx = canvas.getContext('2d')!;
		const colours = new NumberGenerator();
		ctx.clearRect(0, 0, this.size, this.size);
		this.drawNode(canvas, ctx, colours, 0, 0, this.root, this.size);
	}
	
	private drawNode(
		canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D,
		colours: NumberGenerator,
		x: number, y: number,
		node: QDAGNode | null, size: number,
	) {
		if (node !== null) {
			if (size === 1) {
				ctx.fillStyle = `hsl(${colours.assign() * gRatio}turn, 100%, 50%)`;
				ctx.fillRect(x, y, 1, 1);
			} else {
				const s = size / 2;
				this.drawNode(canvas, ctx, colours, x, y, node.tl, s);
				if (node.tr === node.tl) ctx.drawImage(canvas, x, y, s, s, x+s, y, s, s);
				else this.drawNode(canvas, ctx, colours, x+s, y, node.tr, s);
				if (node.bl === node.tl) ctx.drawImage(canvas, x, y, s, s, x, y+s, s, s);
				else if (node.bl === node.tr) ctx.drawImage(canvas, x+s, y, s, s, x, y+s, s, s);
				else this.drawNode(canvas, ctx, colours, x, y+s, node.bl, s);
				if (node.br === node.tl) ctx.drawImage(canvas, x, y, s, s, x+s, y+s, s, s);
				else if (node.br === node.tr) ctx.drawImage(canvas, x+s, y, s, s, x+s, y+s, s, s);
				else if (node.br === node.bl) ctx.drawImage(canvas, x, y+s, s, s, x+s, y+s, s, s);
				else this.drawNode(canvas, ctx, colours, x+s, y+s, node.br, s);
			}
		}
	}
}
