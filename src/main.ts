import VisualQuadDAG from './VisualQuadDAG';

const SIZE = 64;
const SCALE = 8;

const canvas = document.createElement('canvas');
canvas.width = canvas.height = SIZE;
canvas.style.width = canvas.style.height = `${SIZE * SCALE}px`;
canvas.style.imageRendering = 'crisp-edges';
document.body.appendChild(canvas);

const quadDAG = new VisualQuadDAG(SIZE);

canvas.onmousedown = e => {
	const x = Math.floor(e.offsetX / SCALE);
	const y = Math.floor(e.offsetY / SCALE);
	if (e.buttons === 1) quadDAG.set(x, y, true);
	else if (e.buttons === 2) quadDAG.set(x, y, false);
	quadDAG.draw(canvas);
};

canvas.onmousemove = e => {
	const x = Math.floor(e.offsetX / SCALE);
	const y = Math.floor(e.offsetY / SCALE);
	if (e.buttons === 1) quadDAG.set(x, y, true);
	else if (e.buttons === 2) quadDAG.set(x, y, false);
	quadDAG.draw(canvas);
};
